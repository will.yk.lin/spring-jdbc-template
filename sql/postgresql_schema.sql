/*
 Navicat Premium Data Transfer

 Source Server         : v
 Source Server Type    : PostgreSQL
 Source Server Version : 90415
 Source Host           : volumio
 Source Database       : contactdb
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 90415
 File Encoding         : utf-8

 Date: 11/16/2017 16:17:27 PM
*/

-- ----------------------------
--  Sequence structure for seq_contact_id
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."seq_contact_id";
CREATE SEQUENCE "public"."seq_contact_id" INCREMENT 1 START 5 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."seq_contact_id" OWNER TO "postgres";

-- ----------------------------
--  Sequence structure for seq_student_id
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."seq_student_id";
CREATE SEQUENCE "public"."seq_student_id" INCREMENT 1 START 4 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."seq_student_id" OWNER TO "postgres";

-- ----------------------------
--  Table structure for contact
-- ----------------------------
DROP TABLE IF EXISTS "public"."contact";
CREATE TABLE "public"."contact" (
	"contact_id" int4 NOT NULL DEFAULT nextval('seq_contact_id'::regclass),
	"name" varchar(45) NOT NULL COLLATE "default",
	"email" varchar(45) NOT NULL COLLATE "default",
	"address" varchar(45) NOT NULL COLLATE "default",
	"telephone" varchar(45) NOT NULL COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."contact" OWNER TO "postgres";

-- ----------------------------
--  Records of contact
-- ----------------------------
BEGIN;
INSERT INTO "public"."contact" VALUES ('4', '111', '1@1', '123', '321');
INSERT INTO "public"."contact" VALUES ('5', 'Will', 'will.yk.lin@deltaww.com', '256 Yang Guang Street', '0905');
COMMIT;


-- ----------------------------
--  Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."seq_contact_id" RESTART 6;
ALTER SEQUENCE "public"."seq_student_id" RESTART 5;
-- ----------------------------
--  Primary key structure for table contact
-- ----------------------------
ALTER TABLE "public"."contact" ADD PRIMARY KEY ("contact_id") NOT DEFERRABLE INITIALLY IMMEDIATE;

